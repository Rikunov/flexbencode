package net.flexbencode.stream;

import java.io.*;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import static net.flexbencode.stream.BencodeStreamReader.*;

import net.flexbencode.bencode.BencodeData;
import net.flexbencode.bencode.BencodeType;

public class BencodeStreamReaderTest {

    @Test
    public void testEmptyBencodeData () throws BencodeStreamException, IOException {

        String bencodedData = "";

        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = new ByteArrayInputStream(bencodedData.getBytes("US-ASCII"))) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);

            while(r.hasNext()) {
                throw new IllegalStateException();
            }
        }
    }

    @Test
    public void testStrings () throws BencodeStreamException, IOException {

        String bencodedStrings = "3:cat3:pig4:bird3:dog4:lion8:elephant12:hippopotamus";
        String[] decodedStringsExpected = {"cat", "pig", "bird", "dog", "lion", "elephant", "hippopotamus"};

        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = new ByteArrayInputStream(bencodedStrings.getBytes("US-ASCII"))) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);

            int decodedStringIndex = 0;
            while(r.hasNext()) {
                r.next();
                if (EventType.STRING == r.getEventType()) {
                    String decodedString = r.getString();
                    assertEquals(decodedStringsExpected[decodedStringIndex++], decodedString);
                }
            }
        }
    }

    @Test
    public void testGetStringsMultipleTimes () throws BencodeStreamException, IOException {

        String bencodedStrings = "3:cat3:pig4:bird";
        String[] decodedStringsExpected = {"cat", "pig", "bird"};

        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = new ByteArrayInputStream(bencodedStrings.getBytes("US-ASCII"))) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);

            int decodedStringIndex = 0;
            while(r.hasNext()) {
                r.next();
                if (EventType.STRING == r.getEventType()) {
                    String decodedString;
                    decodedString = r.getString();
                    decodedString = r.getString();
                    decodedString = r.getString();
                    r.getString();
                    assertEquals(decodedStringsExpected[decodedStringIndex++], decodedString);
                }
            }
        }
    }

    @Test
    public void testSkipStrings () throws BencodeStreamException, IOException {

        String bencodedStrings = "3:cat3:pig4:bird3:dog4:lion8:elephant12:hippopotamus";

        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = new ByteArrayInputStream(bencodedStrings.getBytes("US-ASCII"))) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);
            while(r.hasNext()) {
                r.next();
            }
        }
    }

    @Test
    public void testInts () throws BencodeStreamException, IOException {

        String bencodedInts = "i3ei0ei-4ei123ei12ei1000000ei-123456e";
        int[] decodedIntsExpected = {3, 0, -4, 123, 12, 1000000, -123456};

        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = new ByteArrayInputStream(bencodedInts.getBytes("US-ASCII"))) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);

            int decodedIntIndex = 0;
            while(r.hasNext()) {
                r.next();
                if (EventType.INT == r.getEventType()) {
                    long num = r.getInt();
                    assertEquals(decodedIntsExpected[decodedIntIndex++], num);
                }
            }
        }
    }

    @Test(expected = BencodeStreamException.class)
    public void testEmptyBencodedInt () throws BencodeStreamException, IOException {

        String bencodedData = "ie";

        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = new ByteArrayInputStream(bencodedData.getBytes("US-ASCII"))) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);

            while(r.hasNext()) {
                r.next();
            }
        }
    }

    @Test
    public void testGetIntsMultipleTimes () throws BencodeStreamException, IOException {
        String bencodedInts = "i3ei0ei-4ei123ei12ei1000000ei-123456e";
        int[] decodedIntsExpected = {3, 0, -4, 123, 12, 1000000, -123456};

        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = new ByteArrayInputStream(bencodedInts.getBytes("US-ASCII"))) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);

            int decodedIntIndex = 0;
            while(r.hasNext()) {
                r.next();
                if (EventType.INT == r.getEventType()) {
                    long num;
                    num = r.getInt();
                    num = r.getInt();
                    r.getInt();
                    num = r.getInt();
                    r.getInt();
                    assertEquals(decodedIntsExpected[decodedIntIndex++], num);
                }
            }
        }
    }

    @Test
    public void testSkipInts () throws BencodeStreamException, IOException {

        String bencodedInts = "i3ei0ei-4ei123ei12ei1000000ei-123456e";

        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = new ByteArrayInputStream(bencodedInts.getBytes("US-ASCII"))) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);
            while(r.hasNext()) {
                r.next();
            }
        }
    }

    @Test
    public void testIntsWithString () throws BencodeStreamException, IOException {

        String bencodedInts = "i3ei0ei-4ei123ei12e3:cati1000000ei-123456e";
        int[] decodedIntsExpected = {3, 0, -4, 123, 12, 1000000, -123456};
        String strExpected = "cat";

        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = new ByteArrayInputStream(bencodedInts.getBytes("US-ASCII"))) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);

            int decodedIntIndex = 0;
            while(r.hasNext()) {
                r.next();
                if (EventType.INT == r.getEventType()) {
                    long num = r.getInt();
                    assertEquals(decodedIntsExpected[decodedIntIndex++], num);
                }

                if (EventType.STRING == r.getEventType()) {
                    String str = r.getString();
                    assertEquals(strExpected, str);
                }
            }
        }
    }

    @Test
    public void testOneIntElementList () throws BencodeStreamException, IOException {

        String bencodedOneIntElementList = "li3ee";
        EventType[] decodedTypesExpected = {
                EventType.LIST,
                EventType.INT,
                EventType.EOL,
                EventType.EOF};
        int intValueExpected = 3;

        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = new ByteArrayInputStream(bencodedOneIntElementList.getBytes("US-ASCII"))) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);

            int decodedTypeIndex = 0;
            while(r.hasNext()) {
                r.next();

                EventType t = r.getEventType();

                assertEquals(decodedTypesExpected[decodedTypeIndex++], t);

                if (EventType.INT == t) {
                    long num = r.getInt();
                    assertEquals(intValueExpected, num);
                }
            }
        }
    }

    @Test
    public void testMultipleIntsList () throws BencodeStreamException, IOException {

        String bencodedOneIntElementList = "li-3ei-2ei-1ei0ei1ei2ei3ee";
        EventType[] decodedTypesExpected = {
                EventType.LIST,
                EventType.INT,
                EventType.INT,
                EventType.INT,
                EventType.INT,
                EventType.INT,
                EventType.INT,
                EventType.INT,
                EventType.EOL,
                EventType.EOF};
        int[] intValuesExpected = {-3, -2, -1, 0, 1, 2, 3};

        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = new ByteArrayInputStream(bencodedOneIntElementList.getBytes("US-ASCII"))) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);

            int decodedTypeIndex = 0;
            int decodedIntIndex = 0;
            while(r.hasNext()) {
                r.next();

                EventType t = r.getEventType();

                assertEquals(decodedTypesExpected[decodedTypeIndex++], t);

                if (EventType.INT == t) {
                    long num = r.getInt();
                    assertEquals(intValuesExpected[decodedIntIndex++], num);
                }
            }
        }
    }

    @Test
    public void testMultipleIntsListGet () throws BencodeStreamException, IOException {

        String bencodedOneIntElementList = "li-3ei-2ei-1ei0ei1ei2ei3ee";
        long[] intValuesExpected = {-3, -2, -1, 0, 1, 2, 3};

        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = new ByteArrayInputStream(bencodedOneIntElementList.getBytes("US-ASCII"))) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);
            int decodedIntIndex = 0;
            while(r.hasNext()) {
                r.next();
                if (EventType.LIST == r.getEventType()) {
                    List<BencodeData> list = r.getList();
                    for (BencodeData bencodeData : list) {
                        if (BencodeType.INT != bencodeData.getType()) {
                            throw new RuntimeException("Int expected!");
                        }
                        assertEquals(intValuesExpected[decodedIntIndex++], bencodeData.getInt());
                    }
                }
            }
        }
    }

    @Test
    public void test2DList () throws BencodeStreamException, IOException {

        String bencodedOneIntElementList = "lli-3eeli-2eeli-1eeli0eeli1eeli2eeli3eee";
        long[] intValuesExpected = {-3, -2, -1, 0, 1, 2, 3};

        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = new ByteArrayInputStream(bencodedOneIntElementList.getBytes("US-ASCII"))) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);
            int decodedIntIndex = 0;
            while(r.hasNext()) {
                r.next();
                if (EventType.LIST == r.getEventType()) {
                    List<BencodeData> list = r.getList();
                    for (BencodeData bencodeData : list) {
                        if (BencodeType.LIST != bencodeData.getType()) {
                            List<BencodeData> innerList = r.getList();
                            BencodeData num = innerList.get(0);
                            assertEquals(intValuesExpected[decodedIntIndex++], num.getInt());
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testSimpleDict () throws BencodeStreamException, IOException {

        String bencodedOneIntElementList = "d3:cat5:blacke";
        EventType[] decodedTypesExpected = {
                EventType.DICT,
                EventType.STRING,
                EventType.STRING,
                EventType.EOD,
                EventType.EOF};

        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = new ByteArrayInputStream(bencodedOneIntElementList.getBytes("US-ASCII"))) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);

            int decodedTypeIndex = 0;
            while(r.hasNext()) {
                r.next();
                assertEquals(decodedTypesExpected[decodedTypeIndex++], r.getEventType());
            }
        }
    }

    @Test
    public void testSimpleDictGet () throws BencodeStreamException, IOException {
        String bencodedOneIntElementList = "d3:cat5:blacke";
        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = new ByteArrayInputStream(bencodedOneIntElementList.getBytes("US-ASCII"))) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);

            while(r.hasNext()) {
                r.next();
                Map<String, BencodeData> dict = r.getDict();
                BencodeData value = dict.get("cat");
                assertEquals("black", value.getString());
            }
        }
    }

    @Test
    public void decodeTorrentFile () throws BencodeStreamException, IOException {
        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = ClassLoader.class.getResourceAsStream("/test0.torrent")) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);

            while(r.hasNext()) {
                r.next();
                System.out.println(r.getEventType());
                if (EventType.STRING == r.getEventType()) {
                    System.out.println(r.getString());
                } else if (EventType.INT == r.getEventType()) {
                    System.out.println(r.getInt());
                }
            }
        }
    }

    @Test
    public void decodeListsFromTorrentFile () throws BencodeStreamException, IOException {
        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = ClassLoader.class.getResourceAsStream("/test0.torrent")) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);

            while(r.hasNext()) {
                r.next();
                if (EventType.LIST == r.getEventType()) {
                    System.out.println(r.getList());
                }
            }
        }
    }

    @Test
    public void decodeDictFromTorrentFile () throws BencodeStreamException, IOException {
        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream s = ClassLoader.class.getResourceAsStream("/test0.torrent")) {
            BencodeStreamReader r = f.createBencodeStreamReader(s);

            while(r.hasNext()) {
                r.next();
                if (EventType.DICT == r.getEventType()) {
                    Map<String, BencodeData> dict = r.getDict();
                    for (String key : dict.keySet()) {
                        System.out.println("KEY: " + key + " VALUE TYPE: " + dict.get(key).getType());
                    }
                }
            }
        }
    }

    @Test
    public void decodeCoupleOfTorrentFiles () throws BencodeStreamException, IOException {
        for (int i = 0; i < 20; i++) {

            String torrentFilename = String.format("/test%d.torrent", i);


            System.out.println("============================= Deserializing torrent file: " +
                    torrentFilename +
                    " =============================");

            BencodeInputFactory f = new DefaultBencodeInputFactory();
            try (InputStream s = ClassLoader.class.getResourceAsStream(torrentFilename)) {
                BencodeStreamReader r = f.createBencodeStreamReader(s);

                while(r.hasNext()) {
                    r.next();
                    System.out.println(r.getEventType());
                    if (EventType.STRING == r.getEventType()) {
                        System.out.println(r.getString());
                    } else if (EventType.INT == r.getEventType()) {
                        System.out.println(r.getInt());
                    }
                }
            }
        }
    }
}
