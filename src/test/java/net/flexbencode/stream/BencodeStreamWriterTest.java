package net.flexbencode.stream;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

import net.flexbencode.bencode.BencodeData;

public class BencodeStreamWriterTest {
    @Test
    public void testWriteStringsUnchecked () throws BencodeStreamException, IOException {
        String[] strings = {"cat", "dog"};
        BencodeOutputFactory f = new DefaultBencodeOutputFactory();
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            BencodeStreamWriter w = f.createBencodeStreamWriter(output);

            for (String s : strings) {
                w.write(s);
            }

            output.flush();

            assertEquals("3:cat3:dog", new String(output.toByteArray(), "US-ASCII"));
        }
    }

    @Test
    public void testWriteIntsChecked () throws BencodeStreamException, IOException {
        long[] data = {123, 456};
        BencodeOutputFactory f = new DefaultBencodeOutputFactory();
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            BencodeStreamWriter w = f.createBencodeStreamWriter(output);

            for (long n : data) {
                w.write(BencodeData.createBencodeInt(n));
            }

            output.flush();

            assertEquals("i123ei456e", new String(output.toByteArray(), "US-ASCII"));
        }
    }

    @Test
    public void testWriteStringsChecked () throws BencodeStreamException, IOException {
        String[] strings = {"cat", "dog"};
        BencodeOutputFactory f = new DefaultBencodeOutputFactory();
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            BencodeStreamWriter w = f.createBencodeStreamWriter(output);

            for (String s : strings) {
                w.write(BencodeData.createBencodeString(s));
            }

            output.flush();

            assertEquals("3:cat3:dog", new String(output.toByteArray(), "US-ASCII"));
        }
    }

    @Test
    public void testWriteMapUnchecked () throws BencodeStreamException, IOException {
        Map<String, String> map = new HashMap<>();
        map.put("cat", "black");
        map.put("dog", "white");

        BencodeOutputFactory f = new DefaultBencodeOutputFactory();
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            BencodeStreamWriter w = f.createBencodeStreamWriter(output);

            w.write(map);

            output.flush();

            assertEquals("d3:cat5:black3:dog5:whitee", new String(output.toByteArray(), "US-ASCII"));
        }
    }

    @Test
    public void testWriteListUnchecked () throws BencodeStreamException, IOException {
        List<String> list = new ArrayList<>();
        list.add("cat");
        list.add("dog");

        BencodeOutputFactory f = new DefaultBencodeOutputFactory();
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            BencodeStreamWriter w = f.createBencodeStreamWriter(output);

            w.write(list);

            output.flush();

            assertEquals("l3:cat3:doge", new String(output.toByteArray(), "US-ASCII"));
        }
    }
}
