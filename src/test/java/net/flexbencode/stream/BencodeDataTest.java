package net.flexbencode.stream;

import java.util.Date;

import org.junit.Test;

import static org.junit.Assert.*;

import net.flexbencode.bencode.BencodeData;
import net.flexbencode.bencode.BencodeInt;
import net.flexbencode.bencode.BencodeString;
import net.flexbencode.bencode.BencodeType;

public class BencodeDataTest {
    @Test
    public void testIntCreation () {
        BencodeInt d = BencodeData.createBencodeInt(1L);
        assertEquals(BencodeType.INT, d.getType());
        assertEquals(1L, d.getInt());
    }

    @Test
    public void testStringCreation () {
        BencodeString d = BencodeData.createBencodeString("abc");
        assertEquals(BencodeType.STRING, d.getType());
        assertEquals("abc", d.getString());
        assertEquals("abc", d.get());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFailureCreation () {
        BencodeData.createBencodeData(new Date());
    }

    @Test
    public void testIntCreationFromObject () {
        BencodeData d = BencodeData.createBencodeData(1L);
        assertEquals(BencodeType.INT, d.getType());
        assertEquals(1L, d.getInt());
        assertEquals(1L, d.get());
    }

    @Test
    public void testStringCreationFromObject () {
        BencodeData d = BencodeData.createBencodeData("abc");
        assertEquals(BencodeType.STRING, d.getType());
        assertEquals("abc", d.getString());
        assertEquals("abc", d.get());
    }
}
