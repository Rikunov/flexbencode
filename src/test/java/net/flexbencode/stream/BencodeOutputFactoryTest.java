package net.flexbencode.stream;

import org.junit.Test;

import java.io.*;

import static org.junit.Assert.assertNotNull;

public class BencodeOutputFactoryTest {

    @Test
    public void testCreation () throws BencodeStreamException, IOException {
        BencodeOutputFactory f = new DefaultBencodeOutputFactory();
        try (OutputStream output = new ByteArrayOutputStream()) {
            BencodeStreamWriter w = f.createBencodeStreamWriter(output);
            assertNotNull(w);
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreationFailure () throws BencodeStreamException {
        BencodeOutputFactory f = new DefaultBencodeOutputFactory();
        BencodeStreamWriter w = f.createBencodeStreamWriter(null);
    }
}
