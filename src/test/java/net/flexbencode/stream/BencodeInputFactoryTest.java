package net.flexbencode.stream;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class BencodeInputFactoryTest {

    @Test
    public void testCreation () throws BencodeStreamException, IOException {
        BencodeInputFactory f = new DefaultBencodeInputFactory();
        try (InputStream empty = new ByteArrayInputStream(new byte[] {})) {
            BencodeStreamReader r = f.createBencodeStreamReader(empty);
            assertNotNull(r);
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreationFailure () throws BencodeStreamException {
        BencodeInputFactory f = new DefaultBencodeInputFactory();
        BencodeStreamReader r = f.createBencodeStreamReader(null);
    }
}
