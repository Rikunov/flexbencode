package net.flexbencode.stream;

import net.flexbencode.bencode.BencodeData;

public interface BencodeStreamWriter {
    void write (BencodeData data) throws BencodeStreamException;
    void write (Object data) throws BencodeStreamException;
}
