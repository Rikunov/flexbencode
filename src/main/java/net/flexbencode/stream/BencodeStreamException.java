package net.flexbencode.stream;

public class BencodeStreamException extends Exception {
    public BencodeStreamException(String message) {
        super(message);
    }

    public BencodeStreamException(Throwable cause) {
        super(cause);
    }
}
