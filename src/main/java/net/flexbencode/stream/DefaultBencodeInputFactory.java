package net.flexbencode.stream;

import java.io.InputStream;

public class DefaultBencodeInputFactory extends BencodeInputFactory {
    @Override
    public BencodeStreamReader createBencodeStreamReader(InputStream stream) {

        if (null == stream) {
            throw new IllegalArgumentException("No stream");
        }

        return new DefaultBencodeStreamReader(stream);
    }
}
