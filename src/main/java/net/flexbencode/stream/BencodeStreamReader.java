package net.flexbencode.stream;

import java.util.List;
import java.util.Map;

import net.flexbencode.bencode.BencodeData;

/**
 * The BencodeStreamReader class provides a Cursor style API for parsing Bencode stream.
 */
public interface BencodeStreamReader {

    enum EventType {
        BEGIN,  // Begin of stream
        INT,
        STRING,
        LIST, // Begin of List
        DICT, // Begin of Dictionary
        EOL, // End Of List
        EOD, // End Of Dictionary
        EOF // End Of Stream
    }

    public void next() throws BencodeStreamException;
    public boolean hasNext() throws BencodeStreamException;
    public EventType getEventType();

    public String getString() throws BencodeStreamException;
    public long getInt() throws BencodeStreamException;
    public List<BencodeData> getList() throws BencodeStreamException;
    public Map<String, BencodeData> getDict() throws BencodeStreamException;
    public BencodeData getBencodeData() throws BencodeStreamException;
}
