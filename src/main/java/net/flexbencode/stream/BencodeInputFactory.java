package net.flexbencode.stream;

import java.io.InputStream;

public abstract class BencodeInputFactory {
    public abstract BencodeStreamReader createBencodeStreamReader(InputStream stream);
}
