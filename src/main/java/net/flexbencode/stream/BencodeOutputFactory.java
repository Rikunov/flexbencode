package net.flexbencode.stream;

import java.io.OutputStream;

public abstract class BencodeOutputFactory {
    public abstract BencodeStreamWriter createBencodeStreamWriter(OutputStream stream);
}
