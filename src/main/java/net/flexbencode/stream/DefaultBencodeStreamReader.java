package net.flexbencode.stream;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;

import java.nio.charset.Charset;

import java.util.*;

import net.flexbencode.bencode.*;

public class DefaultBencodeStreamReader implements BencodeStreamReader {

    private PushbackInputStream pushBackStream;
    private EventType nextType = EventType.BEGIN;
    private BencodeData<?> data = null;

    final Deque<BencodeCollectionType> hierarchyStack = new ArrayDeque<>();

    private final int LEN_STR_DELIMITER = ':';
    private final int END = 'e'; // End of Int, List or Dictionary

    private enum BencodeCollectionType {
        DICT,
        LIST,
        DICT_KEY
    }

    public DefaultBencodeStreamReader(InputStream stream) {
        this.pushBackStream = new PushbackInputStream(stream);
    }

    @Override
    public void next() throws BencodeStreamException {
        if (EventType.EOF == nextType) {
            throw new NoSuchElementException();
        }

        if (EventType.BEGIN == nextType) {
            nextType = getNextType();
            return;
        }

        if (null == data) {
            switch (nextType) {
                case STRING:
                    try {
                        skipString ();
                    } catch (IOException e) {
                        throw new BencodeStreamException(e);
                    }
                    break;
                case INT:
                    try {
                        skipInt();
                    } catch (IOException e) {
                        throw new BencodeStreamException(e);
                    }
                    break;
                case LIST:
                    try {
                        skipList();
                    } catch (IOException e) {
                        throw new BencodeStreamException(e);
                    }
                    break;
                case EOL:
                    try {
                        skipEndOfList();
                    } catch (IOException e) {
                        throw new BencodeStreamException(e);
                    }
                    break;
                case DICT:
                    try {
                        skipDict();
                    } catch (IOException e) {
                        throw new BencodeStreamException(e);
                    }
                    break;
                case EOD:
                    try {
                        skipEndOfDict();
                    } catch (IOException e) {
                        throw new BencodeStreamException(e);
                    }
                    break;
            }
        } else {
            data = null;
        }

        nextType = getNextType();
    }

    public EventType getEventType () {
        return nextType;
    }

    @Override
    public boolean hasNext() throws BencodeStreamException {

        if (EventType.EOF == nextType) {
            return false;
        }

        if (-1 == tryRead()) {
            nextType = EventType.EOF;
            return false;
        }

        return true;
    }

    private int tryRead () throws BencodeStreamException {
        try {
            int data = pushBackStream.read();
            pushBackStream.unread(data);
            return data;
        } catch (IOException e) {
            throw new BencodeStreamException(e);
        }
    }

    private EventType getNextType () throws BencodeStreamException {
        try {
            int data = pushBackStream.read();
            pushBackStream.unread(data);

            if (-1 == data) {
                return EventType.EOF;
            }

            switch ((char) data) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    expectDictKey ();
                    return EventType.STRING;
                case 'i':
                    expectDictValue(EventType.INT);
                    return EventType.INT;
                case 'l':
                    expectDictValue(EventType.LIST);
                    hierarchyStack.push(BencodeCollectionType.LIST);
                    return EventType.LIST;
                case 'd':
                    expectDictValue(EventType.DICT);
                    hierarchyStack.push(BencodeCollectionType.DICT);
                    return EventType.DICT;
                case 'e':
                    if (hierarchyStack.size() > 0) {
                        BencodeCollectionType collectionType = hierarchyStack.pop();
                        if (BencodeCollectionType.LIST == collectionType) {
                            return EventType.EOL;
                        } else if (BencodeCollectionType.DICT == collectionType) {
                            return EventType.EOD;
                        }
                    }
                    throw new BencodeStreamException("List or Dict end expected");
                default:
                    throw new BencodeStreamException("Unexpected character found");
            }
        } catch (IOException e) {
            throw new BencodeStreamException(e);
        }
    }

    private void expectDictKey () throws BencodeStreamException {
        BencodeCollectionType collectionType = hierarchyStack.peek();
        if (BencodeCollectionType.DICT == collectionType) {
            hierarchyStack.push(BencodeCollectionType.DICT_KEY);
        } else if (BencodeCollectionType.DICT_KEY == collectionType) {
            hierarchyStack.remove(BencodeCollectionType.DICT_KEY);
        }
    }

    private void expectDictValue (EventType type) throws BencodeStreamException {
        BencodeCollectionType collectionType = hierarchyStack.peek();
        if (BencodeCollectionType.DICT == collectionType) {
            throw new BencodeStreamException(String.format("String key expected but %s found", type));
        } else if (BencodeCollectionType.DICT_KEY == collectionType) {
            hierarchyStack.remove(BencodeCollectionType.DICT_KEY);
        }
    }

    public String getString () throws BencodeStreamException {
        if (EventType.STRING != nextType) {
            throw new IllegalStateException("String expected");
        }

        if (null != data) {
            return data.getString();
        }

        data = getBencodeString();

        return data.getString();
    }

    private BencodeString getBencodeString () throws BencodeStreamException {
        StringBuilder buf = new StringBuilder();
        int ubyte;

        try {
            while ((ubyte = pushBackStream.read()) != LEN_STR_DELIMITER) {
                if (ubyte == -1) {
                    throw new EOFException();
                }
                buf.append((char)ubyte);
            }
        } catch (IOException e) {
            throw new BencodeStreamException(e);
        }

        int strLength = Integer.parseInt(buf.toString());
        byte[] strData = new byte[strLength];

        try {
            int bytesRead = pushBackStream.read(strData, 0, strLength);

            if (bytesRead < strLength) {
                throw new EOFException("Unexpected EOF");
            }
        } catch (IOException e) {
            throw new BencodeStreamException(e);
        }

        return BencodeData.createBencodeString(new String(strData, Charset.forName("US-ASCII")));
    }

    private void skipString () throws IOException {
        if (EventType.STRING != nextType) {
            throw new IllegalStateException("String expected");
        }

        StringBuilder buf = new StringBuilder();
        int ubyte;
        while ((ubyte = pushBackStream.read()) != LEN_STR_DELIMITER) {
            if (ubyte == -1) {
                throw new EOFException();
            }
            buf.append((char)ubyte);
        }

        int strLength = Integer.parseInt(buf.toString());
        for (int i = 0; i < strLength; i++) {
            if (-1 == pushBackStream.read()) {
                throw new EOFException("Unexpected EOF");
            }
        }
    }

    public long getInt () throws BencodeStreamException {
        if (EventType.INT != nextType) {
            throw new IllegalStateException("Int expected");
        }

        if (null != data) {
            return data.getInt();
        }

        data = getBencodeInt();

        return data.getInt();
    }

    private BencodeInt getBencodeInt () throws BencodeStreamException {
        try {
            // skip 'i'
            pushBackStream.read();

            StringBuilder buf = new StringBuilder();
            int ubyte;
            while ((ubyte = pushBackStream.read()) != END) {
                if (ubyte == -1) {
                    throw new EOFException();
                }
                buf.append((char)ubyte);
            }

            return BencodeData.createBencodeInt(Long.parseLong(buf.toString()));
        } catch (IOException e) {
            throw new BencodeStreamException(e);
        }
    }

    private void skipInt () throws IOException {
        if (EventType.INT != nextType) {
            throw new IllegalStateException("Int expected");
        }

        // skip 'i'
        int i = pushBackStream.read();
        if ('i' != i) {
            throw new IllegalStateException("Begin Of Int expected");
        }

        int ubyte = pushBackStream.read();
        if ('-' != ubyte && !Character.isDigit(ubyte)) {
            throw new IOException("Digit or minus sign expected while skipping Int but found: " + (char)ubyte);
        }

        while ((ubyte = pushBackStream.read()) != END) {
            if (!Character.isDigit(ubyte)) {
                throw new IOException("Digit expected while skipping Int but found: " + (char)ubyte);
            }

            if (ubyte == -1) {
                throw new EOFException();
            }
        }
    }

    @Override
    public List<BencodeData> getList() throws BencodeStreamException {
        if (EventType.LIST != nextType) {
            throw new IllegalStateException("List expected");
        }

        if (null != data) {
            return data.getList();
        }

        data = getBencodeList();

        return data.getList();
    }

    private BencodeList getBencodeList() throws BencodeStreamException {
        List<BencodeData> list = new ArrayList<>();
        BencodeList bencodeList = BencodeData.createBencodeList(list);

        while (hasNext()) {
            next();
            if (EventType.EOL != getEventType()) {
                list.add(getBencodeDataFromStream());
            } else {
                break;
            }
        }

        try {
            skipEndOfList();
        } catch (IOException e) {
            throw new BencodeStreamException(e);
        }

        return bencodeList;
    }

    public void skipList() throws IOException {
        if (EventType.LIST != nextType) {
            throw new IllegalStateException("List expected");
        }

        // skip 'l'
        pushBackStream.read();
    }

    public void skipEndOfList() throws IOException {
        if (EventType.EOL != nextType) {
            throw new IllegalStateException("End Of List expected");
        }

        // skip 'e'
        pushBackStream.read();
    }

    @Override
    public Map<String, BencodeData> getDict() throws BencodeStreamException {
        if (EventType.DICT != nextType) {
            throw new IllegalStateException("Dict expected");
        }

        if (null != data) {
            return data.getDict();
        }

        data = getBencodeDict();

        return data.getDict();
    }

    public BencodeDict getBencodeDict() throws BencodeStreamException {

        // TODO: check sorted order

        Map<String, BencodeData> dict = new LinkedHashMap<>();
        BencodeDict bencodeDict = BencodeData.createBencodeDict(dict);

        while (hasNext()) {
            next();
            EventType t = getEventType();
            if (EventType.EOD != getEventType()) {
                if (t == EventType.STRING) {
                    final BencodeData key = getBencodeDataFromStream();
                    if (hasNext()) {
                        next();
                        dict.put(key.getString(), getBencodeDataFromStream());
                    } else {
                        throw new IllegalStateException("Dict Value expected");
                    }
                } else {
                    throw new IllegalStateException("Dict String Key expected");
                }
            } else {
                break;
            }
        }

        try {
            skipEndOfDict();
        } catch (IOException e) {
            throw new BencodeStreamException(e);
        }

        return bencodeDict;
    }

    public void skipDict() throws IOException {
        if (EventType.DICT != nextType) {
            throw new IllegalStateException("List expected");
        }

        // skip 'd'
        pushBackStream.read();
    }

    public void skipEndOfDict() throws IOException {
        if (EventType.EOD != nextType) {
            throw new IllegalStateException("Dict expected");
        }

        // skip 'e'
        pushBackStream.read();
    }

    @Override
    public BencodeData getBencodeData() throws BencodeStreamException {
        return null != data ? data : getBencodeDataFromStream();
    }

    private BencodeData getBencodeDataFromStream() throws BencodeStreamException {

        // TODO: data field should not be modified here - refactor it!

        switch(nextType) {
            case STRING:
                return (data = getBencodeString());
            case INT:
                return (data = getBencodeInt());
            case LIST:
                return (data = getBencodeList());
            case DICT:
                return (data = getBencodeDict());
            default:
                throw new IllegalStateException("Unexpected EventType");
        }
    }
}