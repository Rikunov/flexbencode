package net.flexbencode.stream;

import java.io.OutputStream;

public class DefaultBencodeOutputFactory extends BencodeOutputFactory {
    @Override
    public BencodeStreamWriter createBencodeStreamWriter(OutputStream stream) {

        if (null == stream) {
            throw new IllegalArgumentException("No stream");
        }

        return new DefaultBencodeStreamWriter(stream);
    }
}
