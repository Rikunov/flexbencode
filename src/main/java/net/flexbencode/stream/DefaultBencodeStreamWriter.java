package net.flexbencode.stream;

import java.util.List;
import java.util.Map;

import java.io.IOException;
import java.io.OutputStream;

import net.flexbencode.bencode.BencodeData;

public class DefaultBencodeStreamWriter implements BencodeStreamWriter {

    private OutputStream stream;

    public DefaultBencodeStreamWriter(OutputStream stream) {
        this.stream = stream;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void write(BencodeData data) throws BencodeStreamException {
        try {
            switch(data.getType()) {
                case INT:
                    writeBencodeInt(data);
                    break;
                case STRING:
                    writeBencodeString(data);
                    break;
                case LIST:
                    writeList(data.getList());
                    break;
                case DICT:
                    writeDict(data.getDict());
                    break;
                default:
                    throw new IllegalStateException("Non supported type!");
            }
        } catch (IOException e) {
            throw new BencodeStreamException(e);
        }
    }

    private void writeBencodeInt (BencodeData data) throws IOException {
        writeInt(data.getInt());
    }

    private void writeInt (long data) throws IOException {
        stream.write('i');
        stream.write(String.valueOf(data).getBytes("US-ASCII"));
        stream.write('e');
    }

    private void writeBencodeString (BencodeData data) throws IOException {
        writeString(data.getString());
    }

    private void writeString (String str) throws IOException {
        stream.write(String.valueOf(str.length()).getBytes("US-ASCII"));
        stream.write(':');
        stream.write(str.getBytes("US-ASCII"));
    }

    private void writeList (List<BencodeData> list) throws IOException, BencodeStreamException {
        stream.write('l');
        for (BencodeData bencodeData : list) {
            write(bencodeData);
        }
        stream.write('e');
    }

    private void writeDict (Map<String, BencodeData> data) throws IOException, BencodeStreamException {
        stream.write('d');
        for (Map.Entry<String, BencodeData> entry : data.entrySet()) {
            writeString(entry.getKey());
            write(entry.getValue());
        }
        stream.write('e');
    }

    @Override
    public void write(Object data) throws BencodeStreamException {
        try {
            if (data instanceof Number) {
                writeInt(((Number)data).longValue());
            } else if (data instanceof String) {
                writeString((String) data);
            } else if (data instanceof List) {
                write(BencodeData.createBencodeData(data));
            } else if (data instanceof Map) {
                write(BencodeData.createBencodeData(data));
            } else {
                throw new BencodeStreamException(data.getClass().getName() + " is not supported!");
            }
        } catch (IOException e) {
            throw new BencodeStreamException(e);
        }
    }
}