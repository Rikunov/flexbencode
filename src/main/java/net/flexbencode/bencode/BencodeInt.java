package net.flexbencode.bencode;

/**
 * Bencode integral number type
 */
public class BencodeInt extends BencodeData<Long> {
    BencodeInt(Long num) {
        super(num, BencodeType.INT);
    }
}
