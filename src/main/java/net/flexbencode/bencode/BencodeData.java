package net.flexbencode.bencode;

import java.util.*;

public abstract class BencodeData<T> {

    final BencodeType type;
    final T data;

    public static BencodeString createBencodeString (String data) {
        return new BencodeString(data);
    }

    public static BencodeInt createBencodeInt (long num) {
        return new BencodeInt(num);
    }

    public static BencodeList createBencodeList (List<BencodeData> list) {
        return new BencodeList(list);
    }

    public static BencodeDict createBencodeDict (Map<String, BencodeData> dict) {
        return new BencodeDict(dict);
    }

    @SuppressWarnings("unchecked")
    public static BencodeData createBencodeData (Object data) {
        if (data instanceof Number) {
            return createBencodeInt(((Number) data).longValue());
        } else if (data instanceof String) {
            return createBencodeString((String) data);
        } else if (data instanceof List) {
            List<BencodeData> bencodeList = new ArrayList<>();
            for (Object d : (List)(data)) {
                bencodeList.add(BencodeData.createBencodeData(d));
            }
            return createBencodeList(bencodeList);
        } else if (data instanceof Map) {
            Map<String, BencodeData> map = new HashMap<>();
            Set<Map.Entry> entries = ((Map)(data)).entrySet();
            for (Map.Entry e : entries) {
                Object key = e.getKey();
                Object value = e.getValue();
                if (key instanceof String) {
                    map.put((String) key, createBencodeData(value));
                }
            }
            return createBencodeDict(map);
        } else {
            throw new IllegalArgumentException(data.getClass().getName() + " can't be bencoded!");
        }
    }

    BencodeData(T data, BencodeType type) {
        this.type = type;
        this.data = data;
    }

    public BencodeType getType() {
        return type;
    }

    public T get() {
        return data;
    }

    public String getString() {
        if (BencodeType.STRING == type) {
            return (String) data;
        }

        throw new IllegalStateException();
    }

    public long getInt() {
        if (BencodeType.INT == type) {
            return (Long) data;
        }

        throw new IllegalStateException();
    }

    @SuppressWarnings("unchecked")
    public List<BencodeData> getList() {
        if (BencodeType.LIST == type) {
            return (List<BencodeData>) data;
        }

        throw new IllegalStateException();
    }

    @SuppressWarnings("unchecked")
    public Map<String, BencodeData> getDict() {
        if (BencodeType.DICT == type) {
            return (Map<String, BencodeData>) data;
        }

        throw new IllegalStateException();
    }

    @Override
    public String toString() {
        return "BencodeData{" +
                "type=" + type +
                ", data=" + data +
                '}';
    }
}