package net.flexbencode.bencode;

public class BencodeString extends BencodeData<String> {
    BencodeString(String data) {
        super(data, BencodeType.STRING);
    }
}
