package net.flexbencode.bencode;

/**
 * Bencode data types
 */
public enum BencodeType {
    INT,
    STRING,
    LIST,
    DICT,
}