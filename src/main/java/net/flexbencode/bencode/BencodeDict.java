package net.flexbencode.bencode;

import java.util.Map;

public class BencodeDict extends BencodeData<Map<String, BencodeData>> {
    BencodeDict(Map<String, BencodeData> data) {
        super(data, BencodeType.DICT);
    }
}
