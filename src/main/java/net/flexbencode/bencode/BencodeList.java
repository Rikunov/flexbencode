package net.flexbencode.bencode;

import java.util.List;

public class BencodeList extends BencodeData<List<BencodeData>> {
    BencodeList(List<BencodeData> data) {
        super(data, BencodeType.LIST);
    }
}
